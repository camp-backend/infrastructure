# Bootcamp - Government Digital Services - 2022

## Cloud Infrastructure Provisioning for deploying application on ECS Cluster Fargate

<img src="./public/public-private.png" width="50%" height="50%">
<img src="./public/public-only.png" width="50%" height="50%">

## Prerequisites
There are a couple of things you need for this workshop:
- Docker
  - Using Docker Compose to familiarise with container communication
  - Docker Desktop for [Windows](https://docs.docker.com/desktop/windows/install/) | [macOS](https://docs.docker.com/desktop/mac/install/)
  - Docker Engine and Docker Compose for [Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04) | [Arch Linux](https://wiki.archlinux.org/title/docker)
- AWS Account
  - An AWS console will be used to run the cloudformation scripts
  - Resources to be created in this tutorial
    - VPC
    - Subnets
    - Internet Gateway
    - Route Tables
    - Load Balancer
    - Target Group
    - Security Group
    - ECS Cluster
    - ECS Task Definitions
    - IAM Role and Policies

## What's Next?

Once you're ready, let's get started with the [Exercises](https://gds-engineering-bootcamp.gitlab.io/documentation/docs/introduction/GettingStarted)
